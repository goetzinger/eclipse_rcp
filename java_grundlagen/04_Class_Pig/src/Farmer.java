
public class Farmer {
	
	void feedPig(Pig pig){
		pig.eat();
	}
	
	void feedPig(IndustrialPig pig){
		System.out.println("industrial");
		pig.eat();
	}

}
