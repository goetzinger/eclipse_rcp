public class FarmTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String name = "Miss Piggy";
		Pig missPiggy = new Pig(10.0f, name);
		System.out.println("I create a pig with name " + missPiggy.getName()
				+ " with a weight of " + missPiggy.getWeight());

		missPiggy.eat();
		missPiggy.eat();
		missPiggy.eat();

		Pig misterPig = new Pig(11.0f, "Mister Pig");
		misterPig.walk();
		System.out.println(misterPig.toString());

		System.out.println("Miss Piggy has a weight of "
				+ missPiggy.getWeight());
		missPiggy.grunt();

		// Wenn weight public sichtbar
		// float weightOfPiggy = missPiggy.weight;
	}
}
