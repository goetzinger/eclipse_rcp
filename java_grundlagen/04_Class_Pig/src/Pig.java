
public class Pig{
	
	//Class-Attribute 1 for all objects 
	private static final String DEFAULT_NAME = "Nobody";
	private static final float MAX_WEIGHT = 200f;
	private static final float MIN_WEIGHT = 10.0f;

	//Instance Attributes 1 per object
	private float weight;
	protected final String name;
	
	public Pig(){
		this(MIN_WEIGHT);
	}
	
	public Pig(float weightOfBirth){
		this(weightOfBirth,DEFAULT_NAME);
	}
	
	public Pig(int weightOfBirth){
		this((float)weightOfBirth);
	}
	
	public Pig(float weightOfBirth, String name)
	{
		if(name != null)
			this.name = name;
		else
			this.name = DEFAULT_NAME;
		this.setWeight(weightOfBirth);
	}
	
	public String getName() {
		return name;
	}
	
	public float getWeight() {
		return weight;
	}
	
	public void eat(){
		setWeight(this.weight + 1);
	}
	
	private void setWeight(float newWeight){
		if(newWeight < MAX_WEIGHT && newWeight >= MIN_WEIGHT)
			this.weight = newWeight;
	}
	
	public void walk(){
		setWeight(this.weight -1);
	}
	
	public void grunt(){
		System.out.println(this.getName() + " says oink");
	}

	@Override
	public String toString() {
		return "Pig [weight=" + weight + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Float.floatToIntBits(weight);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pig other = (Pig) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(weight) != Float.floatToIntBits(other.weight))
			return false;
		return true;
	}
	
	
	
	
	
}