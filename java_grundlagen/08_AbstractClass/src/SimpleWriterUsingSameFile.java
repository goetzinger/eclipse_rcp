
public class SimpleWriterUsingSameFile extends AbstractFileWriter{

	@Override
	protected byte[] getFormattedContent(String toWrite) {
		return toWrite.getBytes();
	}

	@Override
	protected String getFileName() {
		return "C:\\test.txt";
	}

}
