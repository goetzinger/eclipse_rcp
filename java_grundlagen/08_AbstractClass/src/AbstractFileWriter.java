import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public abstract class AbstractFileWriter {

	public void write(String toWrite) {
		File f = new File(getFileName());
		FileOutputStream outStream = null;
		try {
			outStream = new FileOutputStream(f);
			outStream.write(getFormattedContent(toWrite));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(outStream != null)
				try {
					outStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	protected abstract byte[] getFormattedContent(String toWrite);

	protected abstract String getFileName();

}
