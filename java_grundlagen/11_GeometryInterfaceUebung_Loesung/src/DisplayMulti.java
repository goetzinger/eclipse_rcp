
public class DisplayMulti {
	
	private Geometry[] geometries = new Geometry[10];
	private int counter =0;
	
	public DisplayMulti(Geometry firstOne){
		this.add(firstOne);
	}
	
	public void add(Geometry g){
		if(counter < geometries.length)
			geometries[counter++] = g;
		else
			throw new IllegalStateException("Array voll!");
	}

}
