
public class Display {

	private Geometry geometry;
	
	public Display(Geometry g) throws NoGeometryAvailableException {
		if(g == null)
			throw new NoGeometryAvailableException();
		geometry = g;
	}
	
	public void paint(){
		System.out.println(geometry.getCoordinates());
	}

}
