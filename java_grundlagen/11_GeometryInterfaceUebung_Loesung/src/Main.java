
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Point point = new Point(10.0,10.0);
		Display display = new Display(point);
		display.paint();
		
		Rectangle rect = new Rectangle(point, new Point(20,20));
		Display display4Rect = new Display(rect);
		display4Rect.paint();
		
		try{
		Display displayThrowingError = new Display(null);
		displayThrowingError.paint();
		}
		catch(NoGeometryAvailableException e)
		{
			e.printStackTrace();
		}
		
//		//Or displaying a Circle
//		Circle circle = new Circle(10.0,10.0,2.0);
//		Display display4Circle = new Display(circle);
//		display4Circle.paint();

	}

}
