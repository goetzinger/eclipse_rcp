
public interface Geometry {
	
	String getCoordinates();

}
