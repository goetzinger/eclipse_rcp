
public class Rectangle implements Geometry{
	
	private Point leftUpper;
	private Point rightBottom;
	
	public Rectangle(Point leftUpper, Point rightBottom) {
		super();
		this.leftUpper = leftUpper;
		this.rightBottom = rightBottom;
	}

	@Override
	public String getCoordinates() {
		return leftUpper.getCoordinates() + " : " + rightBottom.getCoordinates();
	}
	
	

}
