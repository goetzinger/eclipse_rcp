public class MainKontrollstrukturen {

	public static void main(String[] args) {
		boolean b = true;

		if (b == true)
			System.out.println("Wahr");

		else
			System.out.println("Falsch");

		// IF with more than one line
		if (!b) {
			System.out.println("Nochmal falsch");
			System.out.println("Block Teil 2");
		}

		int zahl = 10;

		if (zahl < 10) {
			System.out.println("Zahl kleiner 10");
		} else if (zahl < 20) {
			System.out.println("Zahl kleiner 20");
		} else if (zahl < 30) {
			System.out.println("Zahl kleiner 30");
		} else {
			System.out.println("Zahl gr��er gleich 30");
		}

		while (b) {
			System.out.println("kopfgesteuert");
		}
		do {
			System.out.println("fussgesteuert");
		} while (b);
		
			//einmalig;schleifeninvariante;nach jeder der Iteration
		for(int i = 0;i < 10;i++){
			
		}
	}

}
