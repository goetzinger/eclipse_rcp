import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ListExample {

	/**
	 * @param args
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 * @throws IllegalArgumentException 
	 */
	public static void main(String[] args) throws IllegalArgumentException, SecurityException, NoSuchFieldException {
		List liste = new ArrayList();
		liste.add("Hallo");
		liste.add(new Integer(2));
		
		Integer integer = (Integer) liste.get(1);
		Integer gel�scht = (Integer) liste.remove(1);
		
		
		List pointList = new ArrayList();
		for(int i = 0; i < 10; i++){
			pointList.add(new Point(i,i));
		}
		Point point = (Point) pointList.get(5);
		Class<? extends Point> classPoint = point.getClass();
		try {
			classPoint.getDeclaredField("x").set(point, 5);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ArrayList<Point> pointListTypisiert = new ArrayList<Point>();
		for(int i = 0; i < 10; i++){
			pointListTypisiert.add(new Point(i,i));
		}
		//pointListTypisiert.add(new Integer(1));
		Point point2 = pointListTypisiert.get(5);
		
		List<Geometry> geometryListTypisiert = new ArrayList<Geometry>();
		for(int i = 0; i < 10; i++){
			geometryListTypisiert.add(new Point(i,i));
		}
		Geometry pointGeo = geometryListTypisiert.get(5);
		
		Iterator<Geometry> iterator = geometryListTypisiert.iterator();
		while(iterator.hasNext()){
			Geometry next = iterator.next();
			System.out.println(next);
		}
	}

}
