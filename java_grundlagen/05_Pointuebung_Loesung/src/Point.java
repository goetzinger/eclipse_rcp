
public class Point {
	
	public static final double MAX_Y = 100.0;
	public static final double MAX_X = 100.0;
	public static final double MIN_Y = .0;
	public static final double MIN_X = .0;
	private static int pointCounter = 0;
	
	private double x;
	private double y;
	private int pointNr;
	
	public Point() {
		this(MIN_X);
	}

	public Point(double x) {
		this(x,MIN_Y);
	}

	public Point(double x, double y) {
		this.pointNr = ++pointCounter;
		setX(x);
		setY(y);
	}
	
	private void setY(double y) {
		if(y < MIN_Y)
			this.y = MIN_Y;
		else if(y > MAX_Y)
			this.y = MAX_Y;
		else
			this.y = y;
		
	}

	private void setX(double x) {
		if(x < MIN_X)
			this.x = MIN_X;
		else if(x > MAX_X)
			this.x = MAX_X;
		else
			this.x = x;
	}
	
	public void right() {
		setX(x + 1);
	}
	
	public void left() {
		setX(x - 1);
	}
	
	public static int getCurrentNumberOfPointObjects(){
		return pointCounter;
	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + ", pointNr=" + pointNr + "]";
	}
}
