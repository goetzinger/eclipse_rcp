import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class FileWriter{
	
	Formatter formatter;
	
	public FileWriter(Formatter formatter) {
		super();
		this.formatter = formatter;
	}

	public void write(String toWrite, String fileName) {
		File f = new File(fileName);
		FileOutputStream outStream = null;
		try {
			outStream = new FileOutputStream(f);
			outStream.write(formatter.getFormatedContent(toWrite));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(outStream != null)
				try {
					outStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
}
