
public class WriterClient {
	
	public static void main(String[] args) {
		HTMLFormatter formatter = new HTMLFormatter();
		FileWriter fileWriter = new FileWriter(formatter);
		fileWriter.write("Hallo Welt", "C:\\hello.html");
		
		
		XMLFormatter formatterXML = new XMLFormatter();
		FileWriter fileWriter2 = new FileWriter(formatterXML);
		fileWriter2.write("Hallo Welt", "C:\\hello.xml");
		
	}

}
