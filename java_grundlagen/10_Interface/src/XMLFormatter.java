
public class XMLFormatter implements Formatter{

	@Override
	public byte[] getFormatedContent(String content2Format){
		String formattedContent = "<?xml >\n<content>" +
				content2Format+
				"</content>";
		return formattedContent.getBytes();
	}
	
}
