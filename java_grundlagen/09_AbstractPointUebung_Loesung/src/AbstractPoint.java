
public abstract class AbstractPoint {
	
	public static final double MAX_Y = 100.0;
	public static final double MAX_X = 100.0;
	public static final double MIN_Y = .0;
	public static final double MIN_X = .0;
	private static int pointCounter = 0;
	
	private double x;
	private double y;
	private int pointNr;
	
	
	public AbstractPoint() {
		this(MIN_X);
	}

	public AbstractPoint(double x) {
		this(x,MIN_Y);
	}

	public AbstractPoint(double x, double y) {
		this.pointNr = ++pointCounter;
		setX(x);
		setY(y);
	}
	
	protected void setY(double y) {
		double distanceFromCenterToBounds = getDistance();
		if(y - distanceFromCenterToBounds < MIN_Y)
			this.y = MIN_Y + distanceFromCenterToBounds;
		else if(y + distanceFromCenterToBounds > MAX_Y)
			this.y = MAX_Y -distanceFromCenterToBounds;
		else
			this.y = y;
		
	}

	protected void setX(double x) {
		double distanceToBoundsFromCenter = getDistance();
		
		if(x -distanceToBoundsFromCenter < MIN_X)
			this.x = MIN_X + distanceToBoundsFromCenter;
		else if(x + distanceToBoundsFromCenter > MAX_X)
			this.x = MAX_X - distanceToBoundsFromCenter;
		else
			this.x = x;
	}
	
	public void right() {
		setX(x + 1);
	}
	
	public void left() {
		setX(x - 1);
	}
	
	protected abstract double getDistance();
	
	public static int getCurrentNumberOfPointObjects(){
		return pointCounter;
	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + ", pointNr=" + pointNr + "]";
	}
}
