
public class Circle extends AbstractPoint {
	
	private double radius;

	public Circle(double x, double y, double radius) {
		this.radius = radius;
		super.setX(x);
		super.setY(y);
	}

	@Override
	protected double getDistance() {
		return radius;
	}

}
