import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class ZahlenratenApplikation {

	public static void main(String[] args) throws IOException {

		int randomNumber = getRandomNumber();

		boolean hasToBeCanceled = false;
		boolean numberGuessed = false;

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));

		while (!hasToBeCanceled && !numberGuessed) {
			String nextInput = reader.readLine();
			if (nextInput.intern() == "exit") {
				hasToBeCanceled = true;
			} else {
				int numberOfUser = Integer.parseInt(nextInput);
				if (numberOfUser == randomNumber) {
					System.out.println("Zahl erraten");
					numberGuessed = true;
				} else if (numberOfUser > randomNumber) {
					System.out.println("Zahl zu gro�");
				} else
					System.out.println("Zahl zu klein");

			}
		}
	}

	private static int getRandomNumber() {
		Random rand = new Random();
		int randomNumber = rand.nextInt(100);
		return randomNumber;
	}

}
