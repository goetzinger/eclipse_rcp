import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



public class CircleUnitTest {

	
	private Point circle = new Circle(99,99,2);
	
	@BeforeClass
	public static void initSomethingOnce(){
		//Initialize Something for all Tests once
	}
	
	@Before
	public void initSomethingElse(){
		
	}
	
	@Test
	public void circleShouldBeInBounds(){
		Assert.assertEquals(new Circle(98,98,2),circle);
	}
	
	
	@Test
	public void circleShouldBeInBoundsAfterMovingLeft(){
		circle.left();
		Assert.assertEquals(new Circle(97,98,2),circle);
	}
	
	@Test
	public void circleShouldBeInBoundsAfterMovingUp(){
		circle.down();
		Assert.assertEquals(new Circle(98,97,2),circle);
	}
	
	@Test
	public void circleShouldBeInBoundsAfterMovingRight(){
		circle.right();
		Assert.assertEquals(new Circle(98,98,2),circle);
	}

}
