
public class Point {
	
	public static final double MAX_Y = 100.0;
	public static final double MAX_X = 100.0;
	public static final double MIN_Y = .0;
	public static final double MIN_X = .0;
	private static int pointCounter = 0;
	
	private double x;
	private double y;
	private int pointNr;
	
	public Point() {
		this(MIN_X);
	}

	public Point(double x) {
		this(x,MIN_Y);
	}

	public Point(double x, double y) {
		this.pointNr = ++pointCounter;
		setX(x);
		setY(y);
	}
	
	protected void setY(double y) {
		if(y < MIN_Y)
			this.y = MIN_Y;
		else if(y > MAX_Y)
			this.y = MAX_Y;
		else
			this.y = y;
		
	}

	protected void setX(double x) {
		if(x < MIN_X)
			this.x = MIN_X;
		else if(x > MAX_X)
			this.x = MAX_X;
		else
			this.x = x;
	}
	
	public void right() {
		setX(x + 1);
	}
	
	public void left() {
		setX(x - 1);
	}
	
	public static int getCurrentNumberOfPointObjects(){
		return pointCounter;
	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + ", pointNr=" + pointNr + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pointNr;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	public void down() {
		setY(y-1);
		
	}
	
	
}
