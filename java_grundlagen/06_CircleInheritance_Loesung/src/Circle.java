public class Circle extends Point {

	private double radius;

	public Circle(double x, double y, double radius) {
		this.radius = radius;
		setX(x);
		setY(y);
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", " + super.toString() + "]";
	}

	@Override
	protected void setY(double y) {
		if (y + radius > MAX_Y)
			super.setY(MAX_Y - radius);
		else if (y - radius < MIN_Y)
			super.setY(MIN_Y + radius);
		else
			super.setY(y);
	}

	@Override
	protected void setX(double x) {
		if (x + radius > MAX_X)
			super.setX(MAX_X - radius);
		else if (x - radius < MIN_X)
			super.setX(MIN_X + radius);
		else
			super.setX(x);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(radius);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Circle other = (Circle) obj;
		if (Double.doubleToLongBits(radius) != Double
				.doubleToLongBits(other.radius))
			return false;
		return true;
	}

	
}
