import junit.framework.Assert;

import org.junit.Test;


public class PigTest {
	
	Pig testPig = new Pig("MissPiggy",10.0f);

	@Test
	public void initialPigShouldHaveInitialWeight() {
		Assert.assertEquals(10.0f, testPig.getWeight());
	}
	
	@Test
	public void pigShouldHaveOneKiloMoreAfterEatingSomething() {
		testPig.eat();
		Assert.assertEquals(11.0f, testPig.getWeight());
	}

}
