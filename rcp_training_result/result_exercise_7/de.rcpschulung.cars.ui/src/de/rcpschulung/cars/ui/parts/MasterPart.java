package de.rcpschulung.cars.ui.parts;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import de.rcpschulung.cars.domain.Car;
import de.rcpschulung.cars.domain.ICarSelectionVoter;
import de.rcpschulung.cars.domain.ICarService;

public class MasterPart {

	@Inject
	IEventBroker broker;

	@Inject
	private ICarService carService;

	@Inject
	private IExtensionRegistry registry;

	private IEclipseContext context;

	@Inject
	public void setSelection(MWindow wnd) {
		if (wnd != null) {
			this.context = wnd.getContext();
			context.declareModifiable("selected_car");
		}
	}

	@PostConstruct
	public void init(Composite parent) {
		parent.setLayout(new FillLayout());

		ListViewer listViewer = new ListViewer(parent);

		listViewer.setContentProvider(ArrayContentProvider.getInstance());
		List<Car> staticPool = carService.getCars();
		listViewer.setInput(staticPool);
		listViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Car) element).getMake() + " " + ((Car) element).getModel();
			}
		});
		listViewer.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			Car selectedElement = (Car) selection.getFirstElement();
			if (allTheExtensionsAreWillingToDoNotification(selectedElement))
				context.modify("selected_car", selectedElement);
		});
		listViewer.setSelection(new StructuredSelection(staticPool.get(1)));

	}

	private boolean allTheExtensionsAreWillingToDoNotification(Car selectedCar) {
		IConfigurationElement[] configurationElementsFor = registry
				.getConfigurationElementsFor("de.rcpschulung.cars.selectionvoter");
		for (IConfigurationElement extensionConfig : configurationElementsFor) {
			try {
				Object extension = extensionConfig.createExecutableExtension("classname");
				if (extension instanceof ICarSelectionVoter) {
					ICarSelectionVoter voter = (ICarSelectionVoter) extension;
					boolean canProceed = voter.vote(selectedCar);
					if (!canProceed)
						return false;
				}
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return true;
	}

}
