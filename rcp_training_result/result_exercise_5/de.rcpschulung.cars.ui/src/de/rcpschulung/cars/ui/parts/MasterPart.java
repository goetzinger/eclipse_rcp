package de.rcpschulung.cars.ui.parts;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import de.rcpschulung.cars.ui.domain.Car;
import de.rcpschulung.cars.ui.domain.CarServiceMock;

public class MasterPart {
	
	
	@PostConstruct
	public void init(Composite parent) {
		parent.setLayout(new FillLayout());
		
		ListViewer listViewer = new ListViewer(parent);
	
		listViewer.setContentProvider(ArrayContentProvider.getInstance());
		listViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				// TODO Auto-generated method stub
				
			}
		});
		listViewer.setInput(CarServiceMock.getStaticPool());
		listViewer.setLabelProvider(new LabelProvider () {
			@Override
			public String getText(Object element) {
				Car carToPresent = (Car)element;
				return carToPresent.getMake() + " " + carToPresent.getModel();
			}
		});
		
		
	}

}
