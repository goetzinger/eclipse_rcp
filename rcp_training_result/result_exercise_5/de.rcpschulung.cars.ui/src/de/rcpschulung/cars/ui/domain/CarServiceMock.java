package de.rcpschulung.cars.ui.domain;

import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CarServiceMock {
	
	private static List<Car> staticPool = new LinkedList<Car>();
	
	
	static {
		Calendar lastYear = Calendar.getInstance();
		lastYear.add(Calendar.YEAR, -1);
		staticPool.add(new Car("Tesla", "Model 3", 300, 225, lastYear, "Hans", lastYear));
		Calendar twoYearsAgo = Calendar.getInstance();
		twoYearsAgo.add(Calendar.YEAR, -2);
		staticPool.add(new Car("VW", "Golf", 130, 195, twoYearsAgo, "Jan", lastYear));
		staticPool.add(new Car("BMW", "I3", 300, 225, Calendar.getInstance(), "Hans", Calendar.getInstance()));
	}
	
	public static List<Car> getStaticPool(){
		return Collections.unmodifiableList(staticPool);
	}

}
