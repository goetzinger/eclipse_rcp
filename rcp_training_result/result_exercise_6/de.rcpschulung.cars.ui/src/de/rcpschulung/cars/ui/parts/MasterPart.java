package de.rcpschulung.cars.ui.parts;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import de.rcpschulung.cars.ui.domain.Car;
import de.rcpschulung.cars.ui.domain.CarServiceMock;

public class MasterPart {

	@Inject
	IEventBroker broker;

	@PostConstruct
	public void init(Composite parent) {
		parent.setLayout(new FillLayout());

		ListViewer listViewer = new ListViewer(parent);

		listViewer.setContentProvider(ArrayContentProvider.getInstance());
		listViewer.setInput(CarServiceMock.getStaticPool());
		listViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Car) element).getMake() + " " + ((Car) element).getModel();
			}
		});
		listViewer.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection)event.getSelection();
			Car selectedElement = (Car) selection.getFirstElement();
			Map<String,Object> content = new HashMap<>();
			content.put("id", selectedElement.getId());
			broker.post(CustomEvents.TOPIC_CARS_SELECTION, content);
		});

	}

}
