package de.rcpschulung.cars.ui.parts;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.rcpschulung.cars.ui.domain.Car;
import de.rcpschulung.cars.ui.domain.CarServiceMock;

public class MetaPart {

	private Composite parent;
	private Label creationUserLabel;
	private Text creationUserText;
	private Label creationDate;
	private Text creationDateText;
	private Label lastModifiedDateLabel;
	private Text lastModifiedDateText;
	private DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

	@PostConstruct
	public void init(Composite parent) {
		parent.setLayout(new FormLayout());
		this.parent = parent;
		getCreationUserLabel();
		getCreationUserText();
		getCreationDateLabel();
		getCreationDateText();
		getLastModifiedLabel();
		getLastModfiedText();
		setCarDetailsInUI(CarServiceMock.getStaticPool().get(0));
	}
	@Inject
	@Optional
	private void subscribeAndHandle (@UIEventTopic(CustomEvents.TOPIC_CARS_SELECTION) Map<String, Object> contents) {
		setCarDetailsInUI(CarServiceMock.getById((String) contents.get("id")));
	}


	private void setCarDetailsInUI(Car car) {
		creationUserText.setText(car.getCreationUser());
		creationDateText.setText(formatter.format(car.getCreationDate().getTime()));
		lastModifiedDateText.setText(formatter.format(car.getLastModifiedDate().getTime()));

	}

	private Text getCreationUserText() {
		if (creationUserText == null) {
			creationUserText = new Text(parent, SWT.SINGLE | SWT.BORDER);
			creationUserText.setTextLimit(300);
			creationUserText.setEditable(false);
			creationUserText.setText("Admin");
			FormData layoutData = new FormData();
			layoutData.left = new FormAttachment(getCreationUserLabel(), 50);
			layoutData.top = new FormAttachment(getCreationUserLabel(), 0, SWT.CENTER);
			layoutData.right = new FormAttachment(70, 0);
			creationUserText.setLayoutData(layoutData);
		}
		return creationUserText;
	}

	private Label getCreationUserLabel() {
		if (creationUserLabel == null) {
			creationUserLabel = new Label(parent, SWT.PUSH);
			creationUserLabel.setText("Created by:");

			FormData data = new FormData();
			data.top = new FormAttachment(0, 20);
			data.left = new FormAttachment(0, 10);
			creationUserLabel.setLayoutData(data);
		}
		return creationUserLabel;
	}

	private Text getCreationDateText() {
		if (creationDateText == null) {
			creationDateText = new Text(parent, SWT.SINGLE | SWT.BORDER);
			creationDateText.setTextLimit(300);
			creationDateText.setEditable(false);
			creationDateText.setText(formatter.format(Calendar.getInstance().getTime()));
			FormData layoutData = new FormData();
			layoutData.left = new FormAttachment(getCreationUserText(), 0, SWT.LEFT);
			layoutData.top = new FormAttachment(getCreationDateLabel(), 0, SWT.CENTER);
			layoutData.right = new FormAttachment(70, 0);
			creationDateText.setLayoutData(layoutData);
		}
		return creationDateText;
	}

	private Label getCreationDateLabel() {
		if (creationDate == null) {
			creationDate = new Label(parent, SWT.PUSH);
			creationDate.setText("Created at:");

			FormData data = new FormData();
			data.top = new FormAttachment(getCreationUserLabel(), 30);
			data.left = new FormAttachment(0, 10);
			creationDate.setLayoutData(data);
		}
		return creationDate;
	}

	private Text getLastModfiedText() {
		if (lastModifiedDateText == null) {
			lastModifiedDateText = new Text(parent, SWT.SINGLE | SWT.BORDER);
			lastModifiedDateText.setTextLimit(300);
			lastModifiedDateText.setEditable(false);
			lastModifiedDateText.setText(formatter.format(Calendar.getInstance().getTime()));
			FormData layoutData = new FormData();
			layoutData.left = new FormAttachment(getCreationDateText(), 0, SWT.LEFT);
			layoutData.top = new FormAttachment(getLastModifiedLabel(), 0, SWT.CENTER);
			layoutData.right = new FormAttachment(70, 0);
			lastModifiedDateText.setLayoutData(layoutData);
		}
		return lastModifiedDateText;
	}

	private Label getLastModifiedLabel() {
		if (lastModifiedDateLabel == null) {
			lastModifiedDateLabel = new Label(parent, SWT.PUSH);
			lastModifiedDateLabel.setText("Last Modified at:");

			FormData data = new FormData();
			data.top = new FormAttachment(getCreationDateLabel(), 30);
			data.left = new FormAttachment(0, 10);
			lastModifiedDateLabel.setLayoutData(data);
		}
		return lastModifiedDateLabel;
	}

}
