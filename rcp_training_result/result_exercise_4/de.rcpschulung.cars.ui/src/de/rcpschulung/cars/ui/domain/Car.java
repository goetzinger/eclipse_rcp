package de.rcpschulung.cars.ui.domain;

import java.util.Calendar;
import java.util.UUID;

public class Car {
	
	private String id = UUID.randomUUID().toString();
	
	private String make;
	
	private String model;
	
	private int hp;
	
	private int maxKmH;
	
	private Calendar creationDate;
	
	private String creationUser;
	
	private Calendar lastModifiedDate;
	
	public Car() {
	}
	
	public Car(String make, String model, int hp, int maxKmH, Calendar creationDate, String creationUser,
			Calendar lastModifiedDate) {
		super();
		this.make = make;
		this.model = model;
		this.hp = hp;
		this.maxKmH = maxKmH;
		this.creationDate = creationDate;
		this.creationUser = creationUser;
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getMaxKmH() {
		return maxKmH;
	}

	public void setMaxKmH(int maxKmH) {
		this.maxKmH = maxKmH;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreationUser() {
		return creationUser;
	}

	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}

	public Calendar getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Calendar lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	


}
