package de.rcpschulung.cars.service;

import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import de.rcpschulung.cars.domain.Car;
import de.rcpschulung.cars.domain.CarService;

@Component
public class CarServiceMock implements CarService{
	
	private List<Car> staticPool = new LinkedList<Car>();
	
	
	public CarServiceMock() {
		Calendar lastYear = Calendar.getInstance();
		lastYear.add(Calendar.YEAR, -1);
		staticPool.add(new Car("Tesla", "Model 3", 300, 225, lastYear, "Hans", lastYear));
		Calendar twoYearsAgo = Calendar.getInstance();
		twoYearsAgo.add(Calendar.YEAR, -2);
		staticPool.add(new Car("VW", "Golf", 130, 195, twoYearsAgo, "Jan", lastYear));
		staticPool.add(new Car("BMW", "I3", 300, 225, Calendar.getInstance(), "Hans", Calendar.getInstance()));
	}
	
	public List<Car> getCars(){
		return Collections.unmodifiableList(staticPool);
	}

	public Car getById(String id) {
		return staticPool.stream().filter(c -> c.getId().equals(id)).findAny().get();
	}

	@Override
	public void addCar(Car newCar) {
		this.staticPool.add(newCar);
	}
	@Override
	public void removeCar(Car toRemove) {
		this.staticPool.remove(toRemove);
	}
}
