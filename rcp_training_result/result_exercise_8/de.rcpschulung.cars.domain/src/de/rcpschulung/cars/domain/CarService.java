package de.rcpschulung.cars.domain;

import java.util.List;

public interface CarService {
	
	List<Car> getCars();

	
	Car getById(String id);
	
	void addCar(Car newCar);
	
	void removeCar(Car toRemove);
	
}
