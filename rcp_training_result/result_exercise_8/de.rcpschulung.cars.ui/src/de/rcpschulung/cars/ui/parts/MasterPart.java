package de.rcpschulung.cars.ui.parts;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import de.rcpschulung.cars.domain.Car;
import de.rcpschulung.cars.domain.CarService;

public class MasterPart {

	@Inject
	private IEventBroker broker;
	
	@Inject
	private CarService carService; 

	private IEclipseContext context;

	@Inject
	public void setSelection(MWindow wnd) {
		if (wnd != null) {
			this.context = wnd.getContext();
			context.declareModifiable("selected_car");
		}
	}

	@PostConstruct
	public void init(Composite parent) {
		parent.setLayout(new FillLayout());

		ListViewer listViewer = new ListViewer(parent);

		listViewer.setContentProvider(ArrayContentProvider.getInstance());
		listViewer.setInput(carService.getCars());
		listViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Car) element).getMake() + " " + ((Car) element).getModel();
			}
		});
		listViewer.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			Car selectedElement = (Car) selection.getFirstElement();
			context.modify("selected_car", selectedElement);
		});
		listViewer.setSelection(new StructuredSelection(((List)listViewer.getInput()).get(0)));

	}

}
