package de.rcpschulung.cars.ui.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import de.rcpschulung.cars.domain.Car;
import de.rcpschulung.cars.domain.CarService;

public class DetailPart {

	private Composite parent;
	private Label makeLabel;
	private Text makeText;
	private Label modelLabel;
	private Text modelText;
	private Label hpLabel;
	private Spinner hpSpinner;
	private Label maxKmHLabel;
	private Spinner maxKmHSpinner;
	private Car selectedCar;
	
	@Inject
	private CarService carService;

	@PostConstruct
	public void init(Composite parent) {
		parent.setLayout(new FormLayout());
		this.parent = parent;
		getMakeLabel();
		getMakeText();
		getModelLabel();
		getModelText();
		getHPLabel();
		getHPSpinner();
		getMaxKmHLabel();
		getMaxKmHSpinner();

		setCarDetailsInUI(selectedCar != null ? selectedCar : carService.getCars().get(0));
	}

	@Inject
	@Optional
	void printMySelection(@Named("selected_car") Car car) {
		if (car != null) {
			this.selectedCar = car;
			if(parent != null)
				this.setCarDetailsInUI(selectedCar);
		}
	}


	private void setCarDetailsInUI(Car car) {
		makeText.setText(car.getMake());
		modelText.setText(car.getModel());
		hpSpinner.setSelection(car.getHp());
		maxKmHSpinner.setSelection(car.getMaxKmH());

	}

	private Text getMakeText() {
		if (makeText == null) {
			makeText = new Text(parent, SWT.SINGLE | SWT.BORDER);
			makeText.setTextLimit(300);
			makeText.setMessage("Type Make here");
			FormData layoutData = new FormData();
			layoutData.left = new FormAttachment(getMakeLabel(), 50);
			layoutData.top = new FormAttachment(getMakeLabel(), 0, SWT.CENTER);
			layoutData.right = new FormAttachment(70, 0);
			makeText.setLayoutData(layoutData);
		}
		return makeText;
	}

	private Label getMakeLabel() {
		if (makeLabel == null) {
			makeLabel = new Label(parent, SWT.PUSH);
			makeLabel.setText("Make:");

			FormData data = new FormData();
			data.top = new FormAttachment(0, 20);
			data.left = new FormAttachment(0, 10);
			makeLabel.setLayoutData(data);
		}
		return makeLabel;
	}

	private Text getModelText() {
		if (modelText == null) {
			modelText = new Text(parent, SWT.SINGLE | SWT.BORDER);
			modelText.setTextLimit(300);
			modelText.setMessage("Type Model here");
			FormData layoutData = new FormData();
			layoutData.left = new FormAttachment(getMakeText(), 0, SWT.LEFT);
			layoutData.top = new FormAttachment(getModelLabel(), 0, SWT.CENTER);
			layoutData.right = new FormAttachment(70, 0);
			modelText.setLayoutData(layoutData);
		}
		return modelText;
	}

	private Label getModelLabel() {
		if (modelLabel == null) {
			modelLabel = new Label(parent, SWT.PUSH);
			modelLabel.setText("Model:");

			FormData data = new FormData();
			data.top = new FormAttachment(getMakeLabel(), 30);
			data.left = new FormAttachment(0, 10);
			modelLabel.setLayoutData(data);
		}
		return modelLabel;
	}

	private Spinner getHPSpinner() {
		if (hpSpinner == null) {
			hpSpinner = new Spinner(parent, SWT.BORDER);
			hpSpinner.setMinimum(10);
			hpSpinner.setMaximum(1000);
			FormData layoutData = new FormData();
			layoutData.left = new FormAttachment(getModelText(), 0, SWT.LEFT);
			layoutData.top = new FormAttachment(getHPLabel(), 0, SWT.CENTER);
			hpSpinner.setLayoutData(layoutData);
		}
		return hpSpinner;
	}

	private Label getHPLabel() {
		if (hpLabel == null) {
			hpLabel = new Label(parent, SWT.PUSH);
			hpLabel.setText("HP:");

			FormData data = new FormData();
			data.top = new FormAttachment(getModelLabel(), 30);
			data.left = new FormAttachment(0, 10);
			hpLabel.setLayoutData(data);
		}
		return hpLabel;
	}

	private Spinner getMaxKmHSpinner() {
		if (maxKmHSpinner == null) {
			maxKmHSpinner = new Spinner(parent, SWT.BORDER);
			maxKmHSpinner.setMinimum(25);
			maxKmHSpinner.setMaximum(1000);
			maxKmHSpinner.setSelection(200);
			FormData layoutData = new FormData();
			layoutData.left = new FormAttachment(getHPSpinner(), 0, SWT.LEFT);
			layoutData.top = new FormAttachment(getMaxKmHLabel(), 0, SWT.CENTER);
			maxKmHSpinner.setLayoutData(layoutData);
		}
		return maxKmHSpinner;
	}

	private Label getMaxKmHLabel() {
		if (maxKmHLabel == null) {
			maxKmHLabel = new Label(parent, SWT.PUSH);
			maxKmHLabel.setText("Max KmH:");

			FormData data = new FormData();
			data.top = new FormAttachment(getHPLabel(), 30);
			data.left = new FormAttachment(0, 10);
			maxKmHLabel.setLayoutData(data);
		}
		return maxKmHLabel;
	}

}
